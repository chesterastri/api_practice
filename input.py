import datetime
import hashlib
import logging
import os
import ssl
import time
import csv
import math
from os.path import basename

from pymongo import ASCENDING, MongoClient

RSS_FILE = './RSS.txt'
DATA_FILE = './ASLintel1000Clean.txt'

def get_data(data_path):
	logging.info('Reading Data File')
	with open(data_path, newline = '') as raw_data:
		data = csv.DictReader(raw_data, delimiter = '\t')
		return list(data)

def get_data_row(raw_dataframe, rss_dataframe, row):
	data_row = {}

	try:
		data_row = {
			'website': raw_dataframe[row]['source'],
			'url': raw_dataframe[row]['url'],
			'title': raw_dataframe[row]['title'],
			'content': raw_dataframe[row]['content'],
			'metadata': {
				'tags': raw_dataframe[row]['hashtag'].split(';'),
				'categories': ['security']
			},
			'updateTimestamp': datetime.datetime.now(),
			'createTimestamp': raw_dataframe[row]['date'],
			'creator': 'Chester'
		}

	except KeyError as e:
		raise KeyError('Configuration data key not found: {}'.format(str(e)))

	try:
		rssid = int(raw_dataframe[row]['RSSid']) - 1
		data_row.update({'websiteUrl': rss_dataframe[rssid]['RSSurl']})
	except KeyError as e:
		raise KeyError('Configuration RSSid not found: {}'.format(str(e)))

	return data_row

def data_insert_db(data, rss, db, pos):
	try:
		new_doc = get_data_row(data, rss, pos)
		result = db.get_collection('intelDocs').insert_one(new_doc)
		new_doc_id = str(result.inserted_id)
		logging.info('Successfully inserted a new document. ID: %s', new_doc_id)

		return new_doc_id

	except:
		raise

def input_data():
	try:
		logging.info('Connecting to mongodb')
		client = MongoClient('localhost', 27017)
		logging.info('Conncetion successful')
		db_obj = client['API_Test']

		data = get_data(DATA_FILE)
		rss = get_data(RSS_FILE)

		for i in range(len(data)):
			doc_id = data_insert_db(data, rss, db_obj, i)

	except:
		raise

	finally:
		# clean up before return or propagate
		logging.info('Closing database connection')
		if 'client' in locals():
			client.close()

	return


def main():
	try:
		input_data()
	except Exception as err:
		logging.error(err)
		logging.error(type(err))
		logging.error(END_WITH_ERROR)
		exit(1)

	logging.info('End normally')
	exit(0)

main()


























