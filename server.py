from flask import Flask
from flask import jsonify
from flask import request
from flask_pymongo import PyMongo

app = Flask(__name__)

DBNAME = 'API_Test'

app.config['MONGO_DBNAME'] = DBNAME
app.config['MONGO_URI'] = 'mongodb://localhost:27017/' + DBNAME

mongo = PyMongo(app)

@app.route('/content', methods=['GET'])
def get_all_stars():
	star = mongo.db.get_collection('intelDocs')
	output = []
	result = star.find({}).distinct('website')
	for s in result:
		output.append({'website' : s})
	return jsonify({'result' : output, 'count' : len(result)})

@app.route('/content/<website>', methods=['GET'])
def get_stars(website):
	star = mongo.db.get_collection('intelDocs')
	print(website)
	output = []
	result = star.find({'website' : website})
	for s in result:
		output.append({'_id': str(s['_id']), 'title': s['title'], 'website': s['website']})
	return jsonify({'result' : output, 'count' : len(output)})

@app.route('/tags/<tag>', methods=['GET'])
def get_tags(tag):
	coll = mongo.db.get_collection('intelDocs')
	output = []
	result = coll.find({'metadata.tags': {'$eq': tag}})
	for s in result:
		output.append({'_id': str(s['_id']), 'title': s['title'], 'website': s['website'], 'metadata': s['metadata']})
	return jsonify({'result' : output, 'count' : len(output)})


if __name__ == '__main__':
    app.run(debug=True)
